#
# Cookbook Name:: app
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory node["app"]["path"] do
    owner "www-data"
    group "www-data"
    mode "0755"
    action :create
    recursive true
end

bash "add_vhost" do
    user "root"
    code <<-EOH
        echo "127.0.0.1 #{node['app']['server_name']}" >>/etc/hosts
        EOH
end

nginx_vhost "app" do
    template "vhost.erb"
    document_root node["app"]["path"]
    server_name node["app"]["server_name"]
    log_path node["nginx"]["log_dir"]
end

include_recipe "mysql::ruby"

mysql_database node["app"]["database"] do
    connection ({:host => "localhost", :username => "root", :password => node["mysql"]["server_root_password"]})
    action :create
end
