include_recipe "chef-npm"

npm_package "bower" do
    version "0.6.2"
    action :install
end
